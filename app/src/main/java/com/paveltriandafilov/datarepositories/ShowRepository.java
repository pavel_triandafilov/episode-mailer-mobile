package com.paveltriandafilov.datarepositories;

import android.annotation.SuppressLint;
import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.os.AsyncTask;
import android.widget.ProgressBar;

import com.paveltriandafilov.apihelper.ApiService;
import com.paveltriandafilov.dao.ShowDao;
import com.paveltriandafilov.db.AppDataBase;
import com.paveltriandafilov.entities.Episode;
import com.paveltriandafilov.entities.Show;
import com.paveltriandafilov.helpers.DateFormaterHelper;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ShowRepository {
    private ShowDao showDao;
    private LiveData<List<Show>> shows;

    private Show show = null;
    private long idShow = -1;

    private List<Episode> todaysEpisodes = new ArrayList<>();
    private MutableLiveData<List<Episode>> episodes = new MutableLiveData<>();

    public ShowRepository(Application app) {
        this.showDao = AppDataBase.getDatabase(app).showDao();
        this.shows = showDao.getAll();
    }

    public LiveData<List<Show>> getAllMyShows() {
        return shows;
    }

    @SuppressLint("StaticFieldLeak")
    public long insert(Show show) {

        new AsyncTask<Show, Void, Long>() {
            @Override
            protected Long doInBackground(Show... shows) {
                return showDao.insert(shows[0]);
            }

            @Override
            protected void onPostExecute(Long id) {
                super.onPostExecute(id);
                idShow = id;
            }
        }.execute(show);

        return idShow;
    }

    @SuppressLint("StaticFieldLeak")
    public Show getShow(long id) {
        new AsyncTask<Long, Void, List<Show>>() {
            @Override
            protected List<Show> doInBackground(Long... longs) {
                return showDao.getShow(longs[0]);
            }

            @Override
            protected void onPostExecute(List<Show> shows) {
                super.onPostExecute(shows);

                if (!shows.isEmpty()) {
                    show = shows.get(0);
                }
            }
        }.execute(id);

        return show;
    }

    public ShowDao getShowDao() {
        return showDao;
    }

    @SuppressLint("StaticFieldLeak")
    public void delete(final Show show) {
        new AsyncTask<Show, Void, Void>() {
            @Override
            protected Void doInBackground(Show... shows) {
                showDao.delete(show);
                return null;
            }
        }.execute(show);
    }

    public MutableLiveData<List<Episode>> getEpisodes() {
        return episodes;
    }

    public void retrieveTodaysEpisodes(final List<Show> shows, final int showIndex, final int page) {
        ApiService.getInstance().getEpisodeMailerApi().getEpisodes(shows.get(showIndex).getId(), page).enqueue(new Callback<List<Episode>>() {
            @Override
            public void onResponse(Call<List<Episode>> call, Response<List<Episode>> response) {
                List<Episode> allPageEpisodes = response.body();


                if (allPageEpisodes != null && !allPageEpisodes.isEmpty()) {
                    for (Episode episode : allPageEpisodes) {
                        if (checkIfEpisodeToday(episode) & !checkIfEpisodesContainsEpisode(episode)) {
                            todaysEpisodes.add(episode);
                        }
                    }
                    retrieveTodaysEpisodes(shows, showIndex, page + 1);
                } else if (showIndex < shows.size() - 1) {
                    retrieveTodaysEpisodes(shows, showIndex + 1, 1);
                }

                if (allPageEpisodes != null && allPageEpisodes.isEmpty() & showIndex == shows.size() - 1) {
                    episodes.setValue(todaysEpisodes);
                }
            }

            @Override
            public void onFailure(Call<List<Episode>> call, Throwable t) {

            }
        });
    }

    private boolean checkIfEpisodeToday(Episode episode) {
        String currentDate = DateFormaterHelper.getSimpleDateFormat(DateFormaterHelper.AIRED_DATE_FORMAT).format(new Date());

        return episode.getFirstAired().equals(currentDate);
    }

    private boolean checkIfEpisodesContainsEpisode(Episode episode) {
        boolean result = false;
        for (Episode e : todaysEpisodes) {
            if (e.getEpisodeName().equals(episode.getEpisodeName())) {
                result = true;
            }
        }

        return result;
    }
}
