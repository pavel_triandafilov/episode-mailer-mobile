package com.paveltriandafilov.entities;

import com.google.gson.annotations.SerializedName;

public class Episode {
    @SerializedName("id")
    private long id;

    @SerializedName("seriesId")
    private long seriesId;

    @SerializedName("episodeName")
    private String episodeName;

    @SerializedName("firstAired")
    private String firstAired;

    @SerializedName("airedSeason")
    private String airedSeason;

    @SerializedName("airedEpisodeNumber")
    private String airedEpisodeNumber;

    @SerializedName("overview")
    private String overview;

    @SerializedName("filename")
    private String banner;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getEpisodeName() {
        return episodeName;
    }

    public void setEpisodeName(String episodeName) {
        this.episodeName = episodeName;
    }

    public String getFirstAired() {
        return firstAired;
    }

    public void setFirstAired(String firstAired) {
        this.firstAired = firstAired;
    }

    public String getOverview() {
        return overview;
    }

    public void setOverview(String overview) {
        this.overview = overview;
    }

    public String getBanner() {
        return banner;
    }

    public void setBanner(String banner) {
        this.banner = banner;
    }

    public String getAiredSeason() {
        return airedSeason;
    }

    public void setAiredSeason(String airedSeason) {
        this.airedSeason = airedSeason;
    }

    public String getAiredEpisodeNumber() {
        return airedEpisodeNumber;
    }

    public void setAiredEpisodeNumber(String airedEpisodeNumber) {
        this.airedEpisodeNumber = airedEpisodeNumber;
    }

    public long getSeriesId() {
        return seriesId;
    }

    public void setSeriesId(long seriesId) {
        this.seriesId = seriesId;
    }
}
