package com.paveltriandafilov.apihelper;

import com.paveltriandafilov.entities.Episode;
import com.paveltriandafilov.entities.Show;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface EpisodeMailerApi {
    @GET("/tvdb/search")
    Call<List<Show>> search(@Query("q") String query);

    @GET("/api/tvdb/shows/{showId}/episodes")
    Call<List<Episode>> getEpisodes(@Path("showId") Long showId, @Query("page") int page);
}
