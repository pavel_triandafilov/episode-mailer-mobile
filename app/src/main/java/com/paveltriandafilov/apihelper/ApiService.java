package com.paveltriandafilov.apihelper;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiService {
    private static final String BASE_URL = "https://episodemailer.com/";
    public static final String BASETVDB_URL = "https://www.thetvdb.com/banners/";

    private static ApiService mInstance;
    private Retrofit mRetrofit;

    private ApiService() {
        mRetrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    public static ApiService getInstance() {
        if (mInstance == null) {
            mInstance = new ApiService();
        }
        return mInstance;
    }

    public EpisodeMailerApi getEpisodeMailerApi() {
        return mRetrofit.create(EpisodeMailerApi.class);
    }
}
