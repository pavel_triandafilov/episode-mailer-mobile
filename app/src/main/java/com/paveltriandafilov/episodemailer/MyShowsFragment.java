package com.paveltriandafilov.episodemailer;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.paveltriandafilov.adapters.ShowsListAdapter;
import com.paveltriandafilov.entities.Show;
import com.paveltriandafilov.viewmodels.MyShowsViewModel;

import java.util.ArrayList;
import java.util.List;

public class MyShowsFragment extends Fragment {
    Context context;

    RecyclerView myShowsRecyclerView;

    LinearLayoutManager layoutManager;
    ShowsListAdapter showsListAdapter;

    private MyShowsViewModel myShowsViewModel;

    List<Show> showList = new ArrayList<>();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        context = getContext();
        myShowsViewModel = ViewModelProviders.of(getActivity()).get(MyShowsViewModel.class);

        myShowsViewModel.getAllMyShows().observe(this, new Observer<List<Show>>() {
            @Override
            public void onChanged(@Nullable List<Show> shows) {
                showList = shows;
                showsListAdapter.setShowsList(showList);
            }
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_my_shows, container, false);

        myShowsRecyclerView = view.findViewById(R.id.my_shows_recycler_view);

        layoutManager = new LinearLayoutManager(context);
        myShowsRecyclerView.setLayoutManager(layoutManager);

        showsListAdapter = new ShowsListAdapter(myShowsViewModel, context, this.getClass().getName());
        myShowsRecyclerView.setAdapter(showsListAdapter);

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        showsListAdapter.setShowsList(showList);
    }
}
