package com.paveltriandafilov.episodemailer;

import android.annotation.SuppressLint;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.paveltriandafilov.adapters.EpisodesListAdapter;
import com.paveltriandafilov.apihelper.ApiService;
import com.paveltriandafilov.entities.Episode;
import com.paveltriandafilov.entities.Show;
import com.paveltriandafilov.helpers.DateFormaterHelper;
import com.paveltriandafilov.viewmodels.MyShowsViewModel;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EpisodesFragment extends Fragment {

    Context context;

    ProgressBar progressBar;
    TextView onEmptyListMessage, todayDate;
    RecyclerView episodesRecycleView;

    LinearLayoutManager layoutManager;
    EpisodesListAdapter episodesListAdapter;

    private MyShowsViewModel myShowsViewModel;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        context = getContext();

        myShowsViewModel = ViewModelProviders.of(getActivity()).get(MyShowsViewModel.class);

        myShowsViewModel.getAllMyShows().observe(this, new Observer<List<Show>>() {
            @SuppressLint("StaticFieldLeak")
            @Override
            public void onChanged(@Nullable List<Show> shows) {
                episodesListAdapter.clearEpisodeList();

                if (shows != null && !shows.isEmpty()) {
                    onEmptyListMessage.setVisibility(TextView.GONE);
                    progressBar.setVisibility(ProgressBar.VISIBLE);

                    myShowsViewModel.getShowRepository().retrieveTodaysEpisodes(shows, 0, 1);
                } else {
                    onEmptyListMessage.setVisibility(TextView.VISIBLE);
                }
            }
        });

        myShowsViewModel.getTodaysEpisodes().observe(this, new Observer<List<Episode>>() {
            @Override
            public void onChanged(@Nullable List<Episode> episodeList) {
                episodesListAdapter.setEpisodeList(episodeList);

                progressBar.setVisibility(ProgressBar.GONE);
            }
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_coming_out, container, false);
        todayDate = view.findViewById(R.id.today_date);

        progressBar = view.findViewById(R.id.episodes_progress_bar);
        episodesRecycleView = view.findViewById(R.id.episodes_recycler_view);

        onEmptyListMessage = view.findViewById(R.id.on_empty_list_message);

        layoutManager = new LinearLayoutManager(context);
        episodesRecycleView.setLayoutManager(layoutManager);

        episodesListAdapter = new EpisodesListAdapter(myShowsViewModel, this.getContext());
        episodesRecycleView.setAdapter(episodesListAdapter);

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();

        todayDate.setText(DateFormaterHelper.getSimpleDateFormat(DateFormaterHelper.HEADING_DATE_FORMAT).format(new Date()));
    }
}
