package com.paveltriandafilov.episodemailer;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;

import com.paveltriandafilov.adapters.TabAdapter;
import com.paveltriandafilov.broadcastreceivers.AlarmReceiver;
import com.paveltriandafilov.helpers.AlarmHelper;

import java.util.Calendar;

public class MainActivity extends FragmentActivity {
    TabAdapter tabAdapter;

    TabLayout tabLayout;
    ViewPager viewPager;

    PendingIntent pendingIntent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        viewPager = findViewById(R.id.viewPager);
        tabLayout = findViewById(R.id.tabLayout);

        tabAdapter = TabAdapter.initTabs(getSupportFragmentManager());

        viewPager.setAdapter(tabAdapter);
        viewPager.setCurrentItem(1);
        tabLayout.setupWithViewPager(viewPager);

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                if (tabLayout.getSelectedTabPosition() == 0) {
                    ((SearchFragment) tabAdapter.getItem(0)).searchView.setIconified(true);
                }
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        Intent alarmIntent = new Intent(this, AlarmReceiver.class);
        pendingIntent = PendingIntent.getBroadcast(this, 0, alarmIntent, 0);
        //AlarmHelper.setAlarmManagerTo12((AlarmManager) getSystemService(Context.ALARM_SERVICE), pendingIntent);
    }
}
