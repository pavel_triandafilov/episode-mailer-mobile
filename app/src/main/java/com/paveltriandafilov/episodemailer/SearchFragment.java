package com.paveltriandafilov.episodemailer;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import com.paveltriandafilov.adapters.ShowsListAdapter;
import com.paveltriandafilov.apihelper.ApiService;
import com.paveltriandafilov.entities.Show;
import com.paveltriandafilov.viewmodels.MyShowsViewModel;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SearchFragment extends Fragment {
    private static final int ITEM_VIEW_CACHE_SIZE = 10;
    private static final int SEARCH_DELAY = 700;

    Context context;

    SearchView searchView;
    ProgressBar searchProgressBar;
    TextView failTextView;
    RecyclerView searchRecycleView;

    LinearLayoutManager layoutManager;
    ShowsListAdapter showsListAdapter;

    private MyShowsViewModel myShowsViewModel;

    Handler handler;
    String searchQuery;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        context = getContext();
        myShowsViewModel = ViewModelProviders.of(getActivity()).get(MyShowsViewModel.class);

        handler = new Handler();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_search, container, false);
        searchRecycleView = view.findViewById(R.id.search_recycler_view);
        searchView = view.findViewById(R.id.search_view);
        searchProgressBar = view.findViewById(R.id.search_progress_bar);
        failTextView = view.findViewById(R.id.fail_text_view);

        layoutManager = new LinearLayoutManager(context);
        searchRecycleView.setLayoutManager(layoutManager);
        searchRecycleView.setItemViewCacheSize(ITEM_VIEW_CACHE_SIZE);

        showsListAdapter = new ShowsListAdapter(myShowsViewModel, context, this.getClass().getName());
        searchRecycleView.setAdapter(showsListAdapter);

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();

        searchView.setOnQueryTextListener(onQueryTextListener);
        performSearch("");
    }

    // this object will listen search query
    SearchView.OnQueryTextListener onQueryTextListener = new SearchView.OnQueryTextListener() {
        @Override
        public boolean onQueryTextSubmit(String query) {
            performSearch(query);
            return false;
        }

        @Override
        public boolean onQueryTextChange(String newText) {
            searchQuery = newText;
            handler.removeCallbacksAndMessages(null);

            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    performSearch(searchQuery);
                }
            }, SEARCH_DELAY);

            return false;
        }
    };

    private void performSearch(String query) {
        failTextView.setVisibility(TextView.GONE);
        failTextView.setText("");
        searchProgressBar.setVisibility(ProgressBar.VISIBLE);

        ApiService.getInstance().getEpisodeMailerApi().search(query).enqueue(new Callback<List<Show>>() {
            @Override
            public void onResponse(Call<List<Show>> call, Response<List<Show>> response) {
                List<Show> shows = response.body();

                assert shows != null;
                if (shows.isEmpty()) {
                    searchProgressBar.setVisibility(ProgressBar.GONE);
                    failTextView.setVisibility(TextView.VISIBLE);
                    failTextView.setText(R.string.nothing_found);
                } else {
                    searchProgressBar.setVisibility(ProgressBar.GONE);

                    int subShowsCount = (shows.size() < ITEM_VIEW_CACHE_SIZE) ? shows.size() : ITEM_VIEW_CACHE_SIZE - 1;
                    showsListAdapter.setShowsList(shows.subList(0, subShowsCount));
                }
            }

            @Override
            public void onFailure(Call<List<Show>> call, Throwable t) {
                searchProgressBar.setVisibility(ProgressBar.GONE);

                failTextView.setVisibility(TextView.VISIBLE);
                failTextView.setText(R.string.check_internet_message);

                Toast.makeText(context, R.string.something_went_wrong_message, Toast.LENGTH_SHORT).show();
            }
        });
    }
}
