package com.paveltriandafilov.db;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

import com.paveltriandafilov.dao.ShowDao;
import com.paveltriandafilov.entities.Show;

@Database(entities = {Show.class}, version = 1)
public abstract class AppDataBase extends RoomDatabase {
    private static final String DB_NAME = "database";

    public abstract ShowDao showDao();

    private static AppDataBase INSTANCE;

    public static AppDataBase getDatabase(final Context context) {
        if (INSTANCE == null) {
            synchronized (AppDataBase.class) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                            AppDataBase.class, DB_NAME).build();
                }
            }
        }
        return INSTANCE;
    }
}
