package com.paveltriandafilov.helpers;

import android.annotation.SuppressLint;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

public class DateFormaterHelper {
    public static final String HEADING_DATE_FORMAT = "EEE, MMM d";
    public static final String AIRED_DATE_FORMAT = "yyyy-MM-dd";

    @SuppressLint("SimpleDateFormat")
    public static DateFormat getSimpleDateFormat(String format) {
        return new SimpleDateFormat(format);
    }
}
