package com.paveltriandafilov.helpers;

import android.app.AlarmManager;
import android.app.PendingIntent;

import java.util.Calendar;

public class AlarmHelper {
    public static void setAlarmManagerTo12(AlarmManager am, PendingIntent pi) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis());
        calendar.set(Calendar.HOUR_OF_DAY, 12);
        calendar.set(Calendar.MINUTE, 0);

        am.setInexactRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), AlarmManager.INTERVAL_DAY, pi);
    }
}
