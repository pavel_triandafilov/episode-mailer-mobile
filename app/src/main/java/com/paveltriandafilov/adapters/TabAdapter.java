package com.paveltriandafilov.adapters;

import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.paveltriandafilov.episodemailer.EpisodesFragment;
import com.paveltriandafilov.episodemailer.MyShowsFragment;
import com.paveltriandafilov.episodemailer.SearchFragment;

import java.util.ArrayList;
import java.util.List;

public class TabAdapter extends FragmentPagerAdapter {
    private static final String SEARCH_SHOW_TAB = "Search show";
    private static final String COMING_OUT_TAB = "Coming out";
    private static final String MY_SHOWS_TAB = "My Shows";

    private List<Fragment> fragmentsList = new ArrayList<>();
    private List<String> fragmentsTitlesList = new ArrayList<>();

    private TabAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        return fragmentsList.get(position);
    }

    @Override
    public int getCount() {
        return fragmentsList.size();
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return fragmentsTitlesList.get(position);
    }

    private void addFragment(Fragment fragment, String title) {
        fragmentsList.add(fragment);
        fragmentsTitlesList.add(title);
    }

    public static TabAdapter initTabs(FragmentManager fm) {
        TabAdapter tabAdapter = new TabAdapter(fm);

        tabAdapter.addFragment(new SearchFragment(), SEARCH_SHOW_TAB);
        tabAdapter.addFragment(new EpisodesFragment(), COMING_OUT_TAB);
        tabAdapter.addFragment(new MyShowsFragment(), MY_SHOWS_TAB);

        return tabAdapter;
    }
}
