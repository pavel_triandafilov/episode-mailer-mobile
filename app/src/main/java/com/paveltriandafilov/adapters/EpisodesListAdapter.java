package com.paveltriandafilov.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.paveltriandafilov.apihelper.ApiService;
import com.paveltriandafilov.entities.Episode;
import com.paveltriandafilov.entities.Show;
import com.paveltriandafilov.episodemailer.R;
import com.paveltriandafilov.helpers.DownloadImageTask;
import com.paveltriandafilov.viewmodels.MyShowsViewModel;

import java.util.ArrayList;
import java.util.List;

public class EpisodesListAdapter extends RecyclerView.Adapter<EpisodesListAdapter.EpisodesViewHolder> {
    private Context context;
    private MyShowsViewModel myShowsViewModel;

    private List<Episode> episodeList = new ArrayList<>();

    public EpisodesListAdapter(MyShowsViewModel myShowsViewModel, Context context) {
        this.myShowsViewModel = myShowsViewModel;
        this.context = context;
    }

    @NonNull
    @Override
    public EpisodesViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.episode_item_layout, parent, false);
        return new EpisodesViewHolder(view, context);
    }

    @SuppressLint("StaticFieldLeak")
    @Override
    public void onBindViewHolder(@NonNull final EpisodesViewHolder holder, int position) {
        final Episode episode = episodeList.get(position);

        long showId = episode.getSeriesId();

        new AsyncTask<Long, Void, List<Show>>() {
            @Override
            protected List<Show> doInBackground(Long... longs) {
                return myShowsViewModel.getShowRepository().getShowDao().getShow(longs[0]);
            }

            @Override
            protected void onPostExecute(List<Show> shows) {
                super.onPostExecute(shows);

                if (!shows.isEmpty()) {
                    holder.bind(episode, shows.get(0));
                }
            }
        }.execute(showId);
    }

    @Override
    public int getItemCount() {
        return episodeList.size();
    }

    public void setEpisodeList(List<Episode> episodeList) {
        this.episodeList = episodeList;
        notifyDataSetChanged();
    }

    public void clearEpisodeList() {
        this.episodeList.clear();
        notifyDataSetChanged();
    }

    static class EpisodesViewHolder extends RecyclerView.ViewHolder {
        TextView episodeName, episodeOverview, showEpisode, ep_schedule;
        ImageView episodeBanner;
        ProgressBar progressBar;

        Context context;

        EpisodesViewHolder(View itemView, Context context) {
            super(itemView);
            this.context = context;

            showEpisode = itemView.findViewById(R.id.show_episode);
            episodeName = itemView.findViewById(R.id.episode_name);
            episodeOverview = itemView.findViewById(R.id.episode_overview);
            progressBar = itemView.findViewById(R.id.on_episode_image_load_progress_bar);
            episodeBanner = itemView.findViewById(R.id.episode_banner);
            ep_schedule = itemView.findViewById(R.id.ep_schedule);
        }

        void bind(Episode episode, final Show show) {
            String showEpisodeFormatText = show.getSeriesName() + " - S" + episode.getAiredSeason() + "E" + episode.getAiredEpisodeNumber();
            showEpisode.setText(showEpisodeFormatText);

            episodeName.setText(episode.getEpisodeName());
            episodeOverview.setText(episode.getOverview());

            ep_schedule.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://episodemailer.com/" + show.getUrl()));
                    context.startActivity(browserIntent);
                }
            });

            if (episode.getBanner().isEmpty()) {
                new DownloadImageTask(episodeBanner, progressBar).execute(ApiService.BASETVDB_URL + show.getBanner());
            } else {
                new DownloadImageTask(episodeBanner, progressBar).execute(ApiService.BASETVDB_URL + episode.getBanner());
            }
        }
    }
}
