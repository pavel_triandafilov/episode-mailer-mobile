package com.paveltriandafilov.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.paveltriandafilov.apihelper.ApiService;
import com.paveltriandafilov.entities.Show;
import com.paveltriandafilov.episodemailer.R;
import com.paveltriandafilov.helpers.DownloadImageTask;
import com.paveltriandafilov.viewmodels.MyShowsViewModel;

import java.util.ArrayList;
import java.util.List;

public class ShowsListAdapter extends RecyclerView.Adapter<ShowsListAdapter.ShowsViewHolder> {
    private final static String SEARCH_FRAGMENT_CLASS = "com.paveltriandafilov.episodemailer.SearchFragment";
    private final static String MYSHOWS_FRAGMENT_CLASS = "com.paveltriandafilov.episodemailer.MyShowsFragment";

    private Context context;
    private MyShowsViewModel myShowsViewModel;
    private String fragmentClass;
    private List<Show> shows = new ArrayList<>();

    public ShowsListAdapter(MyShowsViewModel myShowsViewModel, Context context, String fragmentClass) {
        this.myShowsViewModel = myShowsViewModel;
        this.context = context;
        this.fragmentClass = fragmentClass;
    }

    @NonNull
    @Override
    public ShowsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.show_item_layout, parent, false);
        return new ShowsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ShowsViewHolder holder, int position) {
        holder.bind(shows.get(position), position);
    }

    @Override
    public int getItemCount() {
        return shows.size();
    }

    public void setShowsList(List<Show> shows) {
        this.shows = shows;
        notifyDataSetChanged();
    }

    class ShowsViewHolder extends RecyclerView.ViewHolder {
        TextView showName;
        ImageView showImage;
        Button button;
        ProgressBar progressBar;

        ShowsViewHolder(View itemView) {
            super(itemView);

            showName = itemView.findViewById(R.id.show_name);
            showImage = itemView.findViewById(R.id.show_image);

            switch (fragmentClass) {
                case SEARCH_FRAGMENT_CLASS:
                    button = itemView.findViewById(R.id.add_show_button);
                    break;
                case MYSHOWS_FRAGMENT_CLASS:
                    button = itemView.findViewById(R.id.unfollow_button);
                    break;
            }

            progressBar = itemView.findViewById(R.id.on_show_image_load_progress_bar);
        }

        void bind(Show show, int position) {
            showName.setText(show.getSeriesName());

            if (show.getBanner().isEmpty()) {
                showImage.setVisibility(ImageView.GONE);
            } else {
                new DownloadImageTask(showImage, progressBar).execute(ApiService.BASETVDB_URL + show.getBanner());
            }

            button.setVisibility(Button.VISIBLE);
            button.setTag(position);
            button.setOnClickListener(onClickListener);
        }

        View.OnClickListener onClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int position = (int) v.getTag();

                switch (v.getId()) {
                    case R.id.add_show_button:
                        myShowsViewModel.insert(shows.get(position));

                        Toast.makeText(context, "Now you follow " + shows.get(position).getSeriesName(), Toast.LENGTH_SHORT).show();
                        break;
                    case R.id.unfollow_button:
                        Show showToDelete = myShowsViewModel.getAllMyShows().getValue().get(position);

                        myShowsViewModel.delete(showToDelete);
                }
            }
        };
    }
}
