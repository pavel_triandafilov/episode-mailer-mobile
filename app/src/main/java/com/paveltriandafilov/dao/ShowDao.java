package com.paveltriandafilov.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.paveltriandafilov.entities.Show;

import java.util.List;

@Dao
public interface ShowDao {
    @Query("SELECT * FROM show")
    LiveData<List<Show>> getAll();

    @Query("SELECT * FROM show WHERE id = :id")
    List<Show> getShow(long id);

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    long insert(Show show);

    @Delete
    void delete(Show show);
}
