package com.paveltriandafilov.viewmodels;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;

import com.paveltriandafilov.datarepositories.ShowRepository;
import com.paveltriandafilov.entities.Episode;
import com.paveltriandafilov.entities.Show;

import java.util.List;

public class MyShowsViewModel extends AndroidViewModel {
    private ShowRepository showRepository;
    private LiveData<List<Show>> allMyShows;

    public MyShowsViewModel(Application app) {
        super(app);

        this.showRepository = new ShowRepository(app);
        allMyShows = showRepository.getAllMyShows();
    }

    public LiveData<List<Show>> getAllMyShows() {
        return allMyShows;
    }

    public LiveData<List<Episode>> getTodaysEpisodes(){
        return showRepository.getEpisodes();
    }

    public long insert(Show show) {
        return showRepository.insert(show);
    }

    public void delete(Show show) {
        showRepository.delete(show);
    }

    public ShowRepository getShowRepository() {
        return showRepository;
    }
}
